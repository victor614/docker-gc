FROM python:3.7-alpine as base
FROM base as builder
RUN mkdir /dependenies
WORKDIR /dependenies
COPY requirements.txt /requirements.txt
RUN pip install --prefix="/dependencies" -r /requirements.txt

FROM base
COPY --from=builder /dependencies /usr/local
COPY ./app /app
WORKDIR /app
ENTRYPOINT ["python", "docker-gc.py"]