import argparse

import docker

from dateutil import parser
from datetime import datetime, timedelta, timezone
from pprint import pprint

client = docker.from_env()


def get_all_images() -> list:
    all_tags = []
    for image in client.images.list(all=True):
        create_datetime = image.attrs.get('Created')
        time_delta = datetime.now(timezone.utc) - parser.isoparse(create_datetime)
        for repo_tag in image.attrs.get('RepoTags'):
            all_tags.append(
                {
                    "repo_tag": repo_tag,
                    "age": time_delta
                }
            )
    return all_tags


def is_old_image(minutes: int, container_age: timedelta) -> bool:
    return container_age > timedelta(minutes=minutes)


def classify_images(tags: list) -> dict:
    repository = {}
    for tag in tags:
        repo_tag = tag.get('repo_tag')
        repo = repo_tag.split(":")[0]
        if repository.get(repo):
            repository[repo].append(tag)
        else:
            repository[repo] = []
            repository[repo].append(tag)
    return repository


def get_images_to_be_deleted(minutes: int, repositories: dict) -> list:
    tags = []
    for repo in repositories.keys():
        if len(repositories[repo]) > 3:
            for tag in repositories[repo][3:]:
                if is_old_image(minutes=minutes, container_age=tag.get('age')):
                    tags.append(tag.get('repo_tag'))
        else:
            continue
    return tags


def delete_images(old_images: list):
    client.images.prune(filters={'dangling': True})
    for image in old_images:
        print("Deleting image {}".format(image))
        client.images.remove(image=image)


def docker_gc(container_age: int):
    all_tags = get_all_images()
    repositories = classify_images(all_tags)
    image_to_be_deleted = get_images_to_be_deleted(minutes=container_age, repositories=repositories)
    print("All images:")
    pprint(repositories)
    print("List of images to be deleted {}".format(image_to_be_deleted))
    delete_images(image_to_be_deleted)


if __name__ == '__main__':
    description = """
    This script will prune all docker images that are older than X minutes, but keeping the latest 3 tags of a image.
    """
    agr_parser = argparse.ArgumentParser(description=description)
    agr_parser.add_argument('Age',
                            type=int,
                            action='store',
                            help='Please enter maximum image age in minutes')
    args = agr_parser.parse_args()
    age = vars(args).get("Age")
    docker_gc(age)
