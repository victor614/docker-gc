# Docker GC

A Python command-line application that is able to prune images that are older than X minutes, but keeping last
three versions (tags) of the image.

## Installation

Use Makefile to build a docker image called `docker-gc` that contains the command-line application.

```bash
make
```

## Usage

Run below command with an integer argument (minutes)

`-v /var/run/docker.sock:/var/run/docker.sock` is needed to mount local docker sock

```bash
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock docker-gc
```

## Example


```bash
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock docker-gc 10 # This will prune the images older than 10 minutes
```